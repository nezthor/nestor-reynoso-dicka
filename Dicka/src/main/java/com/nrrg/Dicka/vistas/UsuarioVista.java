package com.nrrg.Dicka.vistas;

import java.util.HashMap;

import com.nrrg.Dicka.SpringContext;
import com.nrrg.Dicka.herramientas.Constantes;
import com.nrrg.Dicka.herramientas.Errores;
import com.nrrg.Dicka.model.Usuario;
import com.nrrg.Dicka.service.user.UsuarioService;
import com.nrrg.Dicka.service.user.UsuariorServiceImpl;

public class UsuarioVista {

	private UsuarioService usuarioService;
	
	public UsuarioVista(){
		
	}
	
	public Usuario loginUsuario(Usuario usuario){
		Usuario usuarioLogin = null;
		Errores erroresUsuarioLogin = new Errores();
		
		try{
			usuarioService = SpringContext.getBean(UsuariorServiceImpl.class);
			usuarioLogin = usuarioService.buscarUSuario(usuario);
			
			erroresUsuarioLogin.setHayError(Constantes.NOHAYERRORES);
			erroresUsuarioLogin.setDescripcion(Constantes.NODESCRIPCION);
			erroresUsuarioLogin.setTipoError(Constantes.NOTIPOERROR);
			
//			usuarioLogin.setError(erroresUsuarioLogin);
			
		}catch(Exception e){
			usuarioLogin = new Usuario();
//			usuarioLogin.setError(erroresUsuarioLogin);
			
			erroresUsuarioLogin.setHayError(true);
			erroresUsuarioLogin.setDescripcion(e.getMessage());
			erroresUsuarioLogin.setTipoError("Login");
			
			System.out.println(e.getMessage());
		}
		return usuarioLogin;
	}
	
	public boolean validaPin(String pin){
		boolean valido = false;
		if(pin.length() != 4)
			return false;
		for (int i = 0; i < pin.length(); i++) {
			try{
				String valorActual = "" + pin.charAt(i);
				int numeroActual = Integer.parseInt(valorActual);
				valido = true;
				if(numeroActual == 0){
					valido = false;
					break;
				}					
			}catch(Exception e){
				valido = false;
				break;
			}
		}		
		return valido;
		
	}

	public Usuario crearUsuario(String nombre, String apellido, String pin,String numeroId) {
		Usuario resultado = new Usuario();
		resultado.setPrimerNombre(nombre);
		resultado.setSegundoNombre(apellido);
		resultado.setPin(pin);
		resultado.setIdNumero(numeroId);
		usuarioService = SpringContext.getBean(UsuariorServiceImpl.class);
		usuarioService.guardarUsuario(resultado);
		
		
		
		return resultado;
		
	}
}
