package com.nrrg.Dicka.vistas;

import java.util.Scanner;

import com.nrrg.Dicka.herramientas.Constantes;
import com.nrrg.Dicka.model.Usuario;

public class PrincipalVista {

	@SuppressWarnings("resource")
	public PrincipalVista(){
		
		String opcion = "";
		
		while(true){
			
			System.out.println("Binvenido al banco Dicka!");
			System.out.println("1. Iniciar sesion");
			System.out.println("2. Crear cuenta");
			
			Scanner myObj = new Scanner(System.in); 
			opcion = myObj.nextLine();
			
			if(opcion.equals(Constantes.OPCION1) || opcion.equals(Constantes.OPCION2))
				break;
			System.out.println("Por Favor seleccione una opcion que sea valida");
		}
		
		if(opcion.equals(Constantes.OPCION1)){
			new LoginVista();
		}else
			if(opcion.equals(Constantes.OPCION2)){
				String nombre,apellido,pin,numeroId;
				Scanner myObj = new Scanner(System.in);
				while(true){
					System.out.println("Ingrese Nombre, Apellido, PIN y Numero de Identificacion");
										 
					nombre = myObj.nextLine();
					apellido = myObj.nextLine();
					pin = myObj.nextLine();
					numeroId = myObj.nextLine();
					
					UsuarioVista usuarioVista = new UsuarioVista();
					if(usuarioVista.validaPin(pin)){
						Usuario u = usuarioVista.crearUsuario(nombre,apellido,pin,numeroId);
						if(u != null);{
							System.out.println("Usuario creado satisfactoriamente");
							System.out.println("Este es tu ID de Usuario:" + u.getIdUsuario());
							
							break;
						}
					}else{
						System.out.println("El PIN es invalido, reintentar por favor");
					}
				}
				System.out.println("Presiona ENTER para continuar...");
				String enterkey = myObj.nextLine();
				if(enterkey.isEmpty())
					new CuentaVista();
			}		
	}
	
}
