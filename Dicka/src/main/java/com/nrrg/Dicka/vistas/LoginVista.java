package com.nrrg.Dicka.vistas;

import java.util.HashMap;
import java.util.Scanner;

import com.nrrg.Dicka.herramientas.Errores;
import com.nrrg.Dicka.model.Usuario;

public class LoginVista {

	public LoginVista(){
		int idUsuario = 0;
		String pin = "";
		
		int intentos = 0;
		
		Scanner myObj = new Scanner(System.in);
		while(intentos < 3){			
			System.out.println("----- LOGIN ----- ");
			System.out.println("Ingresar IdUsuario y el PIN:");
			 
			idUsuario = myObj.nextInt();		
			System.out.println("PIN:");
			pin = myObj.nextLine();

			Usuario usuarioLogin = new Usuario();
			usuarioLogin.setIdUsuario(idUsuario);
			usuarioLogin.setPin(pin);
			
			UsuarioVista usuarioVista = new UsuarioVista();
			Usuario resultadoLogin = usuarioVista.loginUsuario(usuarioLogin);
//			if(resultadoLogin.getError().isHayError()){
//				System.out.println("Error en el Login, volver a intentarlo por favor");				
//			}else{
			if(resultadoLogin != null){
				if(resultadoLogin.getIdNumero() != null && resultadoLogin.getIdNumero() != "" && resultadoLogin.getPin()!= null && resultadoLogin.getPin().equals(pin)){
					//Login exitoso
					System.out.println("Login Exitoso");
					}else{
						//Login incorrecto
						intentos++;
						System.out.println("Usuario y/o Contraseña incorrectos, vuelve a intentarlo, intento: " + intentos);
						}
					break;
					}
			else{
				intentos++;
				System.out.println("Usuario y/o Contraseña incorrectos, vuelve a intentarlo, intento: " + intentos);
				}
				
//			}
		
		}
		System.out.println("Exceso de Intentos");
		System.out.println("Presione Enterer para continuar...");
		String enterkey = myObj.nextLine();
		if(enterkey.isEmpty())
			new PrincipalVista();
		
	}
}
