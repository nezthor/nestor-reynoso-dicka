package com.nrrg.Dicka.herramientas;

public class Errores {

	private boolean hayError;
	private String tipoError;
	private String descripcion;
	
	public boolean isHayError() {
		return hayError;
	}
	public void setHayError(boolean hayError) {
		this.hayError = hayError;
	}
	public String getTipoError() {
		return tipoError;
	}
	public void setTipoError(String tipoError) {
		this.tipoError = tipoError;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
