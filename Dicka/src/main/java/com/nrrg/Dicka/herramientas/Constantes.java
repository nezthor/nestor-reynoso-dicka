package com.nrrg.Dicka.herramientas;


public class Constantes {		
	public final static String OPCION1 = "1";
	public final static String OPCION2 = "2";
	
	public final static String USUARIOLOGIN = "UsuarioLogin";
	public final static String USUARIOGUARDAR = "UsuarioGuardar";
	
	public final static boolean NOHAYERRORES = false;
	public final static boolean SIHAYERRORES = true;
	public final static String NODESCRIPCION = "SIN DESCRIPCION";
	public final static String NOTIPOERROR = "N/A";
	

}
