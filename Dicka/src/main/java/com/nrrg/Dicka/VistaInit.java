package com.nrrg.Dicka;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.nrrg.Dicka.vistas.PrincipalVista;

@Component
public class VistaInit implements CommandLineRunner{

	@Override
	public void run(String... arg0) throws Exception {
		new PrincipalVista();
		
	}

}
