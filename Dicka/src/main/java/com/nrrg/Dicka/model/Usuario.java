package com.nrrg.Dicka.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table( name = "Usuario" ) 
public class Usuario {

	@Id
	@Column(name = "idUsuario", unique = true, nullable = false)
	@GeneratedValue(generator = "gen")
	private int idUsuario;
	@Column(name = "primerNombre", nullable = false)
	private String primerNombre;
	@Column(name = "segundoNombre", nullable = false)
	private String segundoNombre;
	@Column(name = "pin", nullable = false)
	private String pin;
	@Column(name = "idNumero", nullable = false)
	private String idNumero;
	@Column(name = "balanceActual",nullable = true)	
	private String balanceActual;
	
//	@OneToOne
//	@JoinColumn(name = "idCuenta")
//	private Cuenta cuenta;
//	
//	@OneToMany
//	@JoinColumn(name = "idTransaccion")
//	private Transaccion transaccion;
	
	
	
//public Cuenta getCuenta() {
//		return cuenta;
//	}
//	public void setCuenta(Cuenta cuenta) {
//		this.cuenta = cuenta;
//	}
//	public Transaccion getTransaccion() {
//		return transaccion;
//	}
//	public void setTransaccion(Transaccion transaccion) {
//		this.transaccion = transaccion;
//	}
	//	private Errores error;
//	
//	public Errores getError() {
//		return error;
//	}
//	public void setError(Errores error) {
//		this.error = error;
//	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getIdNumero() {
		return idNumero;
	}
	public void setIdNumero(String idNumero) {
		this.idNumero = idNumero;
	}
	public String getBalanceActual() {
		return balanceActual;
	}
	public void setBalanceActual(String balanceActual) {
		this.balanceActual = balanceActual;
	}
	
	
	
}
