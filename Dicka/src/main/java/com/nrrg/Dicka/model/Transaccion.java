package com.nrrg.Dicka.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "Transaccion" ) 
public class Transaccion {

	@Id
	@Column(name = "idTransaccion", unique = true, nullable = false)
	@GeneratedValue(generator = "gen")
	private int idTransaccion;
	@Column(name = "fecha", nullable = false)
	private Date fecha;
	@Column(name = "tipo", nullable = false)
	private String tipo;
	public int getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(int idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

//	public Usuario getUsuario() {
//		return usuario;
//	}
//
//	public void setUsuario(Usuario usuario) {
//		this.usuario = usuario;
//	}

	@Column(name = "monto", nullable = false)
	private String monto;
	@Column(name = "descripcion", nullable = false)
	private String descripcion;
	
//	@ManyToOne
//	@JoinColumn(name = "idUsuario")
//	private Usuario usuario;
}
