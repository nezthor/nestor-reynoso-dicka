package com.nrrg.Dicka.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table( name = "Cuenta" ) 
public class Cuenta {
	
	@Id
	@Column(name = "idCuenta", unique = true, nullable = false)
	@GeneratedValue(generator = "gen")
	private int idCuenta;
//	public Usuario getUsuario() {
//		return usuario;
//	}
//	public void setUsuario(Usuario usuario) {
//		this.usuario = usuario;
//	}
	@Column(name = "estatus", nullable = false)
	private String estatus;
	@Column(name = "idUsuario", nullable = false)
	private String idUSuario;
	
//	@OneToOne
//	@JoinColumn(name = "idUsuario")
//	private Usuario usuario;
	
	public int getIdCuenta() {
		return idCuenta;
	}
	public void setIdCuenta(int idCuenta) {
		this.idCuenta = idCuenta;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getIdUSuario() {
		return idUSuario;
	}
	public void setIdUSuario(String idUSuario) {
		this.idUSuario = idUSuario;
	}

}
