package com.nrrg.Dicka;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.nrrg.*")
@SpringBootApplication
public class AplicacionPrincipal extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(AplicacionPrincipal.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AplicacionPrincipal.class);
	}
}
