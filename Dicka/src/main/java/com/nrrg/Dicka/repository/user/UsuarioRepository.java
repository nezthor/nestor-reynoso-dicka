package com.nrrg.Dicka.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nrrg.Dicka.model.Usuario;

//por que USER,INTEGER?
//User es el modelo, y el intiger es el tipo de dato que vamos a poner como su ID. el userId va a ser integer
// es paque que genere los datos de DindByid por un Integer

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{

}
