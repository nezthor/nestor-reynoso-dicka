package com.nrrg.Dicka.service.user;

import com.nrrg.Dicka.model.Usuario;

public interface UsuarioService {

	Usuario guardarUsuario(Usuario user);
	
	Usuario buscarUSuario(Usuario user);

}
