package com.nrrg.Dicka.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nrrg.Dicka.model.Usuario;
import com.nrrg.Dicka.repository.user.UsuarioRepository;

@Service
public class UsuariorServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioRepository usuarioRepository;	
	
	@Override
	public Usuario guardarUsuario(Usuario user) {
		Usuario usuarioCreado;
		usuarioRepository.save(user);
//		List<Usuario> listaUsuarios = usuarioRepository.findAll();
		usuarioCreado = usuarioRepository.findAll().get(usuarioRepository.findAll().size() - 1);
		return usuarioCreado;
	}

	@Override
	public Usuario buscarUSuario(Usuario usuario) {
		usuarioRepository.findById(usuario.getIdUsuario());
		return null;
	}
	
}
